﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GUI_6.Model
{
    class Day
    {
        String date;
        DateTime dateValue;
        public Day()
        {
            date = null;
        }

        public void SetDate()
        {
            date = DateTime.Now.ToString();
            dateValue = (Convert.ToDateTime(date.ToString()));
        }

        public DateTime GetDate()
        {
            return dateValue;
        }

    }
}
