﻿using GUI_6.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GUI_6.ViewModel
{
    public class DateVM
    {
        Calendar calendarBlackout;
        Day day;
        //take the day and send it to the view to be crossed out
        public void SetBlackoutDay()
        {
            calendarBlackout = new Calendar();
            day = new Day();
            calendarBlackout.BlackoutDates.Add(new CalendarDateRange(new DateTime(0000, 1, 1), day.GetDate()));
        }

        public Calendar getBlackoutDay()
        {
            return calendarBlackout;
        }
    }
}
